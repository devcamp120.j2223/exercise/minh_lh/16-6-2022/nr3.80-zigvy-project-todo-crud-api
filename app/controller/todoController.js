// import mongoose from './mongoose
const mongoose = require('mongoose');
// import model from './model   
const todoModel = require('../model/todoModel');
// Post create new user
const createTodo = (request, response) => {
    let body = request.body;
    if (!body.title) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "title is required"
        })
    }
    if (!body.completed) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "completed is required"
        })
    }
    //B3: Sư dụng cơ sở dữ liệu
    let todoCreate = {
        _id: mongoose.Types.ObjectId(),
        title: body.title,
        completed: body.completed,
    }
    todoModel.create(todoCreate, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Create todo successfully",
                data: data
            })
        }
    })
}
// get all 
const getAllTodo = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    todoModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all todo success",
                data: data
            })
        }
    })
}
// put all 
const putTodo = (request, response) => {
    //B1: thu thập dữ liệu
    let todoId = request.params.todoId;
    let body = request.body;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "todo ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    let updateTodo = {
        title: body.title,
    }
    todoModel.findByIdAndUpdate(todoId, updateTodo, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update todo success",
                data: data
            })
        }
    })
}
// getUserById all 
const TodoGetById = (request, response) => {
    //B1: thu thập dữ liệu
    let todoId = request.params.todoId;

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: "Todo ID is not valid"
        })
    }

    //B3: thao tác với cơ sở dữ liệu
    todoModel.findById(todoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get todo by id success",
                data: data
            })
        }
    })
}
// deleteUser all 
const deleteTodoById = (request, response) => {
    //B1: thu thập dữ liệu
    let todoId = request.params.todoId;
    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(todoId)) {
        return response.status(400).json({
            status: "Error 400: bad request",
            message: " todo ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    todoModel.findOneAndDelete(todoId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete todo success"
            })
        }
    })
}
// post of userid
//GET POSTS OF USER
const getTodoOfUser = (request, response) => {
    //B1: thu thập dữ liệu
    let userId = request.params.userId;

    let condition = {};

    if (userId) {
        condition.userId = userId;
    }

    //B2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        response.status(400).json({
            status: "Error 400: Bad Request",
            message: "user ID is not valid"
        })
    }
    //B3: thao tác với cơ sở dữ liệu
    todoModel.find(condition, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            response.status(201).json({
                status: "Success: Get All todos Of users Id: " + userId,
                data: data
            })
        }
    })
}
//getAllPost
const getAllTodoQuery = (request, response) => {
    //B1: thu thập dữ liệu
    //B2: validate dữ liệu
    //B3: thao tác với cơ sở dữ liệu
    let userId = request.query.userId

    todoModel.find(userId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get all todos success",
                data: data
            })
        }
    })
}
//export 
module.exports = {
    createTodo, getAllTodo, putTodo, TodoGetById,
    deleteTodoById, getTodoOfUser, getAllTodoQuery
}