// import mongoose
const mongoose = require('mongoose');
// tao scheme
const Schema = mongoose.Schema
// tạo schema model
const photoSchema = new Schema({
    albumId: [
        {
            type: mongoose.Types.ObjectId,
            ref: "album"
        }
    ],
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true,
    },
    thumbnailUrl: {
        type: String,
        required: true,
    }

})

//export modle
module.exports = mongoose.model("photo", photoSchema)