// import mongoose
const mongoose = require('mongoose');
// tao scheme
const Schema = mongoose.Schema
// tạo schema model
const todoSchema = new Schema({
    userId: [
        {
            type: mongoose.Types.ObjectId,
            ref: "users"
        }
    ],
    _id: mongoose.Types.ObjectId,
    title: {
        type: String,
        required: true,
    },
    completed: {
        type: Boolean,
        required: true,
    },

})

//export modle
module.exports = mongoose.model("todo", todoSchema)