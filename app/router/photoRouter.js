// import express
const express = require('express');
// router
const router = express.Router();
// import middleware
const appMiddleware = require('../middleware/appMiddleware');
//import controllers
const { createPhoto, getAllPhoto, putPhoto, PhotoGetById, deletePhotoById, getPhotoOfUser, getAllPhotoQuery } = require('../controller/photoController');
// sử dụng middleware
router.use(appMiddleware);
// Post
router.post("/photos", createPhoto);
//Get
router.get("/photos", getAllPhoto);
//Put
router.put("/photos/:photoId", putPhoto);
//Get by id
router.get("/photos/:photoId", PhotoGetById);
//Delete
router.delete("/photos/:photoId", deletePhotoById);
// getPostsOfUser
router.get("/albums/:albumId/photos", getPhotoOfUser);
// getAllPost
router.get("/photos", getAllPhotoQuery);
//export
module.exports = router;