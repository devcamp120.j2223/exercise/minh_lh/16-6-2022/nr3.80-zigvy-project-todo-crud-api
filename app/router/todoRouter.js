// import express
const express = require('express');
// router
const router = express.Router();
// import middleware
const appMiddleware = require('../middleware/appMiddleware');
//import controllers
const { createTodo, getAllTodo, putTodo, TodoGetById, deleteTodoById, getTodoOfUser, getAllTodoQuery } = require('../controller/todoController');
// sử dụng middleware
router.use(appMiddleware);
// Post
router.post("/todos", createTodo);
//Get
router.get("/todos", getAllTodo);
//Put
router.put("/todos/:todoId", putTodo);
//Get by id
router.get("/todos/:todoId", TodoGetById);
//Delete
router.delete("/todos/:todoId", deleteTodoById);
// getPostsOfUser
router.get("/users/:userId/todos", getTodoOfUser);
// getAllPost
router.get("/todos", getAllTodoQuery);
//export
module.exports = router;