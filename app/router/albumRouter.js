// import express
const express = require('express');
// router
const router = express.Router();
// import middleware
const appMiddleware = require('../middleware/appMiddleware');
//import controllers
const { createAlbum, getAllAlbum, putAlbum,AlbumGetById, deleteAlbumById, getAlbumOfUser, getAllAlbumQuery } = require('../controller/albumController');
// sử dụng middleware
router.use(appMiddleware);
// Post
router.post("/albums", createAlbum);
//Get
router.get("/albums", getAllAlbum);
//Put
router.put("/albums/:albumId", putAlbum);
//Get by id
router.get("/albums/:/albumId", AlbumGetById);
//Delete
router.delete("/albums/:albumId", deleteAlbumById);
// getPostsOfUser
router.get("/users/:userId/albums", getAlbumOfUser);
// getAllPost
router.get("/albums", getAllAlbumQuery);
//export
module.exports = router;